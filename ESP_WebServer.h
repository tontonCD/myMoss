// file: ESP_WebServer.h

#include "settings.h"

/*
#ifndef uint8_t
//#include <types.h> // uint8_t, http://minirighi.sourceforge.net/html/types_8h-source.html
//typedef unsigned char uint8_t;
#endif
*/

#if USE_BUILT_IN_LED 
#if MODE_SERVER
extern int serverBuiltinLedPin;
//#else
//extern int clientBuiltinLedPin; // the server knows clientBuiltinLedPin?? TODO
#endif
#endif

/* libraties */
#if   defined(ESP32  )  // the Board is (e.g.): "ESP32 Dev Module"
// Load Wi-Fi library
# include <WiFi.h>
#elif defined(ESP8266)  // the Board is (e.g.): "LOLIN(WEMO) D1 R2 & mini" 
# include <ESP8266WiFi.h> // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/readme.html
//#include <WiFiClient.h>
//#include <ESP8266WebServer.h>
//#include <ESP8266mDNS.h>
#endif


typedef enum {
    // Client -> Server
    mkLedStatus,
    mkAngle,
    mkDeepSleepValue,
    // web/device -> Server -> Client
    mkSetLedStatus,    
    mkSetDSleepTime,
    mkDoCalib
} MessageKey; // OBSO? should be use in parser, to allow switch

typedef struct { // TO: OBSO, @see Client_MessageSent
    String        mValue;  // {angle=%f, time=%u, ledStatus=0/1 }
    unsigned long mFrom;// myMossIdentifier, ESP.getChipId()
    bool          mTransmit;
    unsigned long mToTarget; // myMossIdentifier
} OneMessage; // OBSO

/* 1) Client -> Server */
typedef struct {
    //String        mValue;  // {angle=%f, time=%u, ledStatus=0/1 }
    //unsigned long mFrom;// myMossIdentifier, ESP.getChipId()

    unsigned long cmsMyMossID;
    float         cmsAngleValue; // data
    unsigned long cmsMesureTime; // data, time for angleValue

    unsigned long cmsSleepTime;  // current value, ms
    bool          cmsLedStatus;  // info
    
    bool          cmsReceived;   // say if we can delete it (TODO: keep the struct instances in an array)
} Client_MessageSent;

// stores the received message on Server,
// note on Server_Received: sent "as this" (to Web Client) if the request is "/data"
typedef struct {
    //String        mValue;  // {angle=%f, time=%u, ledStatus=0/1 }
    //unsigned long mFrom;// myMossIdentifier, ESP.getChipId()
    
    unsigned long smrMyMossID;
    float         smrAngleValue; // data
    unsigned long smrMesureTime; // data, time for angleValue

    unsigned long smrSleepTime;  // current value, ms
    bool          smrLedStatus;  // info
    bool          smrTransmit;    // say if we can delete it, i.e. handled by the PC soft (TODO: keep the struct instances in an array)
} Server_Received; 
// TODO: should write also Server_SentToPC_Data and Server_SentToPC_ClientStatus
// first of all, have two instances: server_Received and server_SentToPC

/* 2) Server -> Client */
// note on Server_Message: client is off while the Sender should send, so data are buffered
typedef struct {
    unsigned long smsSleepTime;     // value to set, ms
    bool          smsLedStatus;     // value to set

    bool          smsSleepTimeShouldSend;
    bool          smsLedStatusShouldSend;
    bool          smsDoCalibrationShouldSend; // do it if true;

    bool          smsReceived;      // say if we can delete it (TODO: keep the struct instances in an array)
} Server_MessageSent;

typedef struct {
    unsigned long cmrSleepTime;  // value to set, ms;
    bool          cmrLedStatus;  // value to set

//    bool          smrDoChangeSleepTime; // if received smsSleepTimeShouldSend;
//    bool          smrDoChangeLedStatus; // if received smsLedStatusShouldSend;
    bool          smrDoCalibration;     // if received smsDoCalibrationShouldSend;
} Client_MessageReceived;

/* ** KEYS ** */
/*   
    unsigned long cmsMyMossID;
    float         cmsAngleValue; // data
    unsigned long cmsMesureTime; // data, time for angleValue

    unsigned long cmsSleepTime;  // current value, ms
    bool          cmsLedStatus;  // info
    
    bool          cmsReceived;   // say if we can delete it (TODO: keep the struct instances in an array)
*/
#define myMossIDKey         "myMossID"
#define angleValueKey       "angle"
#define mesureTimeKey       "time"
#define deepSleepTimeKey    "sleepTime"
#define ledStatusKey        "ledStatus"
#define doCalibrationKey    "doCalibration"
#define receivedKey         "received"  // not used

//#include <HashMap.h> // https://playground.arduino.cc/Code/HashMap/

/** represents data to send or received.
 */
//#ifndef _ServerData_
// data sent from Client to Server
typedef struct ServerData { // still OBSO, but uses "readDataFromSensors"
#if MODE_SERVER
    unsigned long sdChipID; // the ESP8266 chip ID as a 32-bit integer
#endif

    unsigned long sdTime;
    bool          sdIsValid;


#if USE_GYRO_SENSOR || USE_GYRO_READER 
// should be defined as: USE_GYRO_SENSOR for client, USE_GYRO_READER for server
    float sdAngle;
#endif
#if USE_DHT_SENSOR || USE_DHT_READER
    float humidity;
    float temperature; 
#endif

    static ServerData readDataFromSensors();

    String buildData()    { // TODO: return (char *)?
        // model: {angle=%f, time=%u}
        String result = String("{")
#if USE_GYRO_SENSOR || USE_GYRO_READER
        + "angle:" + sdAngle + ", " 
#endif
        + "time:" + sdTime + "}";
        return result;
    }

    OneMessage buildOneMessage();
} TServerData;

/** data sent from Server to Client
 *  they are called "flags", because theu never push to the Client, because the Client can be in DeepSleep,
 *  instead, the Client requests them.
 *  The value are set from an interface, acceding to Server
 */
typedef struct ServerFlags {
    unsigned long sdChipID; // the ESP8266 chip ID as a 32-bit integer; UNUSED yet, will be when we handle many Clients
    
    //bool sfUseDeepSleep;  // try this: if sfSleepTime==0 => use loop()+delay() instead.
    long sfClientSleepTime;
    bool sfClientBuiltInLedState;
};
//#endif // end: #ifndef _ServerData_



/* define MODE_String, for Log */
#if   MODE==modeServer_viaRouter
#   define MODE_String   "modeServer_viaRouter"
#elif MODE==modeServer_asSap
#   define MODE_String   "modeServer_asSap"
#elif MODE==modeClient_NoDeepSleep
#   define MODE_String   "modeClient_NoDeepSleep"
#elif MODE==modeClient_withDeepSleep
#   define MODE_String   "modeClient_withDeepSleep"
#endif


extern const char* http_Common_OK;
extern const char* http_Server_OK_1; // part 1
extern const char* http_Server_OK_2; // part 2
extern const char* html_Server_TOP;
extern const char* html_Server_CSS;
extern const char* html_Server_Heading;

extern const char* html_Server_DHT;
extern const char* html_Server_GYRO;

extern const char* html_Server_ServerSetting;
extern const char* html_Server_ClientSetting_1;
extern const char* html_Server_ClientSetting_2;

extern const char* html_Server_END;


//#ifndef __server
//#define __server
/* Set web server port number to 80 */
//extern WiFiServer server;
//#endif
WiFiClient server_available();

void beginWifi();
//void beginServer_Thru_LocalNetwork();
//void beginServer_As_AccessPoint();
//void  beginClient_Thru_LocalNetwork();
//void  beginClient_As_AccessPoint();

void print_EscapingCRLF(const char *iChars);
void print_EscapingCRLF(String iString);

String extractMsg(String wholeMsg);
String extractWithKey(String theMsg, const char *theKey);


#if MODE_SERVER         // server
void server_checkClient();
#else                   // client
void client_SendData();
#endif
