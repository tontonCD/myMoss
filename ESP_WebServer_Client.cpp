// file: ESP_WebServer_Client.cpp

#include "settings.h"


#if MODE_SERVER==false

#include "ESP_WebServer.h"


// TODO: restore?

// DISPLACED -> "settings.h"

#if USE_DHT_SENSOR
# include "dhtReader.h"
  DHTReader dhtReader;
  DHTData dhtData;
/*
// Please define the Digital pin connected to the DHT sensor:
#   if   defined(ESP32  )   // <- the Board is (e.g.): "ESP32 Dev Module"
const uint8_t DHTPIN = 2;       
#   elif defined(ESP8266)   // <- the Board is (e.g.): "LOLIN(WEMO) D1 R2 & mini" 
const uint8_t DHTPIN = D2; // for D2, but D2 undefined here, consult datasheet     
#   endif
const uint8_t DHTTYPE = DHT22; // real type can be: "DHT 22", "AM2302", "AM2321"
*/
#endif // USE_DHT_SENSOR








#if USE_GYRO_SENSOR
# include "gyroReader.h"
  GYROReader clientGyroReader; // was: "gyroReader"
  GYROData   gyroData;
// DISPLACED
/*
# if    USE_GYRO_SENSOR_MPU6050
  const uint8_t SDA_PIN = D3;
  const uint8_t SDC_PIN = D4;
# elif  USE_GYRO_SENSOR_BNO055
  //const uint8_t SDA_PIN = D3; // Tx
  //const uint8_t SDC_PIN = D4; // SCL, Rx
  // TEST
  const uint8_t SDA_PIN = D1; // Tx
  const uint8_t SDC_PIN = D2; // SCL, Rx
# endif
*/
#endif // USE_GYRO_SENSOR

#if USE_BUILT_IN_LED 
/* CONFLICT, because the default pin is D4, just like SDA_PIN, changing the on/of  will cause the Gyro to CRASH */
bool clientBuiltinLedState = false;
int  clientBuiltinLedPin = LED_BUILTIN; // pin number, =D4, =GPIO2; CONFLICT: D4 is used for SDC_PIN !
int clientLedHIGH = HIGH; // maxValue, (should*) replaces: HIGH
int clientLedLOW =  LOW;  // minValue, (should)  replaces: LOW; not usefull but to simplify searchin occurences 
#endif

#if true // TODO: displace?
/* if error, 
 *  - set to false and edit these values (ssid/passord), 
 *  OR (better):
 *  - duplicate "credentials.example" as "credentials.h" and edit (look for "How TO") */
# include "credentials.h"
#else
#define ssid_LocalNetwork      "your_Router_ssid"
#define password_LocalNetwork  "your_password_for_Router"
#define ssid_AccessPoint       "define_your_AccessPoint_name_here"
#define password_AccessPoint   "define_your_AccessPoint_password_here"
#endif



unsigned long clientDeepsleep = 60000; // 60s

/// TEST // {angle=%f, time=%u}
//MessageKey aKey1 = mkLedStatus;
//OneMessage aMessage1; 

Client_MessageSent client_Sent; // what we will send to the Server
Client_MessageReceived client_Received;// what the Server can reply
const char *serverDisconnectedStr = "Server disconnected ***";  // for log

// message to Send
OneMessage ServerData::buildOneMessage() {

    OneMessage aMessage; 
    // use: client_Sent
    
    //String mValue;  // {angle=%f, time=%u, ledStatus=0/1 }
    //unsigned long mFrom;// myMossIdentifier, ESP.getChipId()
    //bool   mTransmit;
    //unsigned long mToTarget; // myMossIdentifier
/* OK but OBSO
    aMessage.mValue = buildData();
    aMessage.mFrom = ESP.getChipId();
    aMessage.mTransmit = false;
    aMessage.mToTarget = 0; // no target, (or perheaps something like anyServer?)
*/
    return aMessage;
}


ServerData ServerData::readDataFromSensors()   {
    ServerData result;
    unsigned long currentTime = millis();

#if USE_GYRO_SENSOR
    //Serial.print("(readGYRO..");
    GYROData gyroData = clientGyroReader.readGYRO();
    //Serial.print(".ok)..");
    if(gyroData.isValid)
    {
        // LOG
        // Serial.printf(" readData: {angle:%f, time:%u}\n", gyroData.tilt, currentTime);
        
        result.sdTime = currentTime;
        result.sdAngle = gyroData.tilt;
        result.sdIsValid = true;
    } else {
        result.sdIsValid = false;
    }
#else
    result.sdIsValid = false;
#endif // end: if USE_GYRO_SENSOR
    return result;
}



/* Set web server port number, init to 80 */
WiFiServer server(80);
WiFiClient server_available()    {
    return server.available();
}

WiFiClient wifiClient;
String clientResponseHttpHeader = ""; // renamed, was: httpClientHeader
// TODO: should be: server->accesspoint, client->server ???



void beginClient_Thru_LocalNetwork(); // was: beginServer_Thru_LocalNetwork
void beginClient_As_AccessPoint(); // was: beginServer_As_AccessPoint
void beginWifi()  {

#if USE_BUILT_IN_LED 
    pinMode(clientBuiltinLedPin, OUTPUT);

    // Set outputs to LOW
    digitalWrite(clientBuiltinLedPin, LOW); // clientLedLOW
    clientBuiltinLedState = true;
#endif

    /* init SENSORS */
#if USE_DHT_SENSOR
    Serial.println("Using: DHT Sensor");
    dhtReader.beginReader(DHTPIN, DHTTYPE);
#endif

#if USE_GYRO_SENSOR
    Serial.println("Using: GYRO Sensor");
    clientGyroReader.initReader(SDA_PIN, SDC_PIN);
#endif

    
    
#if MODE_SAP    //  USE__SET_SOFT_AP
    beginClient_As_AccessPoint();
#else           // USE__LOCAL_NETWORK
    beginClient_Thru_LocalNetwork();
#endif
}

#if MODE_SAP==false
void beginClient_Thru_LocalNetwork()
{
    Serial.println("Connecting to Local Network");
    
    // set mode to: WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF
    WiFi.mode(WIFI_STA); // WeMo only ?
    
    // Connect to Wi-Fi network with SSID and password
    Serial.printf(" Connecting to %s", ssid_LocalNetwork);

    WiFi.begin(ssid_LocalNetwork, password_LocalNetwork);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    } 
    Serial.println();
    if(WiFi.status() != WL_CONNECTED)
    {
        Serial.println(" Failed to connect");
        return;
    }
    
    // Print local IP address and start web server
    Serial.println(" WiFi connected.");
    Serial.print(" IP address: ");Serial.println(WiFi.localIP());
    
    server.begin();
    Serial.println(" HTTP server started");
}
#endif // MODE_SAP==false

#if MODE_SAP
void beginClient_As_AccessPoint()
{
    Serial.println("Connecting as Access Point");
    
    // Connect to Wi-Fi network with SSID and password

// change IP Adress (only ESP8286?)
// fails; keep and see...
# if false 
    // https://github.com/esp8266/Arduino/issues/2208
    IPAddress local_IP(192,168,4,22);
    IPAddress gateway(192,168,4,9);
    IPAddress subnet(255,255,255,0);
    Serial.print("Changing IP to: ");Serial.println(local_IP);

    // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-class.html
    // channel - optional parameter to set Wi-Fi channel, from 1 to 13. Default channel = 1
    WiFi.softAPConfig(local_IP, gateway, subnet); // 
    //if (!WiFi.softAP( ssid, password,channel,0))
    if (!WiFi.softAP( ssid_AccessPoint, password_AccessPoint ))
    {
        Serial.println("WiFi.softAP failed.(Password too short?)");
        return;
    }

  
    //IPAddress myIP = WiFi.softAPIP();
    IPAddress myIP = WiFi.softAPIP();
    //Serial.println();
    //Serial.print("AP IP address: ");
    //Serial.println(myIP);

  // doesn work; @see https://esp32.com/viewtopic.php?t=13371
# else
    // Remove the password parameter, if you want the AP (Access Point) to be open
    WiFi.softAP(ssid_AccessPoint, password_AccessPoint);
    IPAddress myIP = WiFi.softAPIP();
# endif

    /* MAC adress */
    uint8_t macAddr[6]; // 24:62:AB:DC:6E:A0 ?
    WiFi.softAPmacAddress(macAddr);
    Serial.printf("MAC address = %02x:%02x:%02x:%02x:%02x:%02x\n", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
    Serial.printf("MAC address = %s\n", WiFi.softAPmacAddress().c_str());
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());

    /* IP */
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    /* in this mode the IP is always 192.168.4.1 (?) */
    
    server.begin();
    Serial.println("Access Point started");
}
#endif // MODE_SAP







/* ***** SERVER ***** */
#if false // Server Only
void server_checkClient()
{
}
#else
/* ***** CLIENT ***** */
unsigned long sendcurrentTime;
unsigned long sendTimetout = 60;
unsigned long sendDelayBetween = 10000; // delay Between two send
unsigned long lastSendTime = millis();



/**  */

void printBoth(const char *iString)   {
    Serial.print(    iString ); 
    wifiClient.print(iString);
}
void printBoth(uint32_t iValue)   {
    Serial.print(    iValue ); 
    wifiClient.print(iValue);
}
void printlnBoth()   {
    Serial.print("\\r\\n"); // double slash for it prints as text
    wifiClient.println();
}



void client_SendData()
{
    sendcurrentTime = millis();
    // wait until lastSendTime+sendDelayBetween:
    if(sendcurrentTime > (lastSendTime+sendDelayBetween) )
    {
        
        
        /* send: */
        // REF: SenderClass::sendTCP, SenderClass::sendFHEM, ...

        Serial.println("** Sending to Server, Connecting...");
        if(wifiClient.connect(serverAdress_LocalNetwork, serverPort_LocalNetwork))
        {

            //Serial.print("Reading Sensors..");
            ServerData serverData_toSend = ServerData::readDataFromSensors();
            //Serial.println("..ok");
            //String theData;
            String messageToSend;

            if(serverData_toSend.sdIsValid)
            {

#if false // OLD
                theData = serverData_toSend.buildData();
                Serial.print(" Sending: "); Serial.print(theData); Serial.println();
#else
                //Serial.print("Building message..");
                client_Sent.cmsMyMossID =   ESP.getChipId();
                client_Sent.cmsAngleValue = serverData_toSend.sdAngle;
                client_Sent.cmsMesureTime = serverData_toSend.sdTime;
                client_Sent.cmsSleepTime = clientDeepsleep;
# if USE_BUILT_IN_LED 
                client_Sent.cmsLedStatus = clientBuiltinLedState;
# endif
                client_Sent.cmsReceived = false;

                //build the message:
                //bool addComma = false;
                messageToSend ="{ ";

                messageToSend += myMossIDKey": "; // "myMossID: ";
                messageToSend += client_Sent.cmsMyMossID;
                messageToSend += ", ";

                messageToSend += "angle: ";
                messageToSend += client_Sent.cmsAngleValue;
                messageToSend += ", ";

                messageToSend += "time: ";
                messageToSend += client_Sent.cmsMesureTime;
                messageToSend += ", ";

                messageToSend += "sleepTime: "; // "sleepTime: "
                messageToSend += client_Sent.cmsSleepTime;
# if USE_BUILT_IN_LED 
                messageToSend += ", ";

                messageToSend += "ledStatus: ";
                messageToSend += client_Sent.cmsLedStatus;
# endif

                messageToSend += " }";
                
                //Serial.println("..ok");
#endif




                // ref: "/setData {angle=10.2, date=5}"

                // printf("GET /setData {angle=%f, time=%u}\n", serverData_toSend.sdAngle, serverData_toSend.sdTime); 

                Serial.print(" Sending: ");
                //printBoth("GET /setData "); printBoth( theData.c_str() ); printlnBoth();
                printBoth("GET /setData "); printBoth( messageToSend.c_str() ); printlnBoth();
              
                printBoth("HTTP/1.1"); printlnBoth();
                
                //wifiClient.printf("Host: %s:%s\r\n", adress, port); // usefull?
                

                printBoth("User-Agent: "); printBoth("mySpindle");  printlnBoth();


                printBoth("Connection: close"); printlnBoth();
                printlnBoth();

                
                Serial.println();
                lastSendTime = sendcurrentTime;

                /* WAIT FOR THE SERVER RESPONSE */
                Serial.println("** Server did respond:");
                Serial.print(" ");
                int timeout = 0;
                #define CONNTIMEOUT 2000
                while (!wifiClient.available() /* && timeout < CONNTIMEOUT */)
                {
                    timeout++;
                    if(timeout > CONNTIMEOUT)   {
                        Serial.println(" (nothing)");
                        break;
                    }
                    delay(1);
                }
                String currentLine = "";
                while (wifiClient.available())
                {
                    char c = wifiClient.read();
                    clientResponseHttpHeader += c;
#if false
                    Serial.write(c);
#else
                    if (c == '\n')  {   // if the byte is a newline character
                        
                        Serial.write("\\n"); // \\n: as text
                         
                         // END OF LINE
                        
                        // if the current line is blank, you got two newline characters in a row.
                        // that's the end of the client HTTP request, so send a response:
                        if (currentLine.length() == 0) {
    
                            Serial.println(); // identation for the log
                                
                            // END OF MESSAGE
                        
#if false // not here, we are the Client!
                            // respond "OK":
                            //Serial.print( "responding: ");Serial.print(http_Common_OK);Serial.println();
                            wifiIncomingClient.println(http_Common_OK); // "HTTP/1.1 200 OK"
#endif    
                        


                            // don't take the whole message for parsing:
                            String theCommand = extractMsg(clientResponseHttpHeader);
                            
                            String cmdValue;
#if USE_BUILT_IN_LED 
                            // builtIn:
                            if (     theCommand.indexOf(ledStatusKey) >= 0) {
                                
                                cmdValue = extractWithKey(theCommand, ledStatusKey);
                                client_Received.cmrLedStatus = cmdValue.toInt();
                                Serial.print(" Led Status, value: "); Serial.print(cmdValue);Serial.print(" (");Serial.print(client_Received.cmrLedStatus);Serial.print("), ");
                                if(client_Received.cmrLedStatus > 0)
                                {
                                    Serial.print(" Setting builtIn led (GPIO: "); Serial.print(clientBuiltinLedPin); Serial.print("), to On");Serial.println();
                                    clientBuiltinLedState = true;
                                    digitalWrite(clientBuiltinLedPin, LOW); // clientLedLOW    // why? but it's the right command...
                                } else
                                {
                                    Serial.print(" Setting builtIn led (GPIO: "); Serial.print(clientBuiltinLedPin); Serial.print("), to Off");Serial.println();
                                    clientBuiltinLedState = false;
                                    digitalWrite(clientBuiltinLedPin, HIGH); // clientLedHIGH   // why? (ditto)    
                                }
                            } // end: ledStatusKey
                            else
#endif
                            if (theCommand.indexOf(deepSleepTimeKey) >= 0) {    // "sleepTime", replaces "deepsleep"
                                cmdValue = extractWithKey(theCommand, deepSleepTimeKey);
                                client_Received.cmrSleepTime = cmdValue.toInt();
                                clientDeepsleep = client_Received.cmrSleepTime;
                                Serial.print(" deepSleepTimeKey, value: "); Serial.print(cmdValue);Serial.print(" (");Serial.print(clientDeepsleep);Serial.println(")");
                            }
                            else if (theCommand.indexOf(doCalibrationKey) >= 0) {
                                client_Received.smrDoCalibration = true;
                                Serial.print(" doCalibrationKey "); Serial.println();
                                // TODO: calibration
                            }
    
    
                            // respond "OK": http_Client_OK
                            // -> NO..., we are the Client

                        } // end: if(currentLine.length() == 0)
                        

                        else { // if you got a newline, then clear currentLine
                            currentLine = "";
                            //Serial.print(" "); // identation for the log
                        }
                    }   // end:  if (c == '\n')
                    
                    else   
                    { 
                        //clientResponseHttpHeader += c;
//                        Serial.write(c);
                        if (c != '\r') {  // if you got anything else but a carriage return character,
                            Serial.write(c);
                            currentLine += c;      // add it to the end of the currentLine
                        } else
                        {
                            Serial.print("\\r");
                        }
                    }
#endif
                } //end: while (wifiClient.available())
                
                // Clear the header variable
                clientResponseHttpHeader = "";
                
                // currentValue = 0;

                // Close the connection
                wifiClient.stop();
                Serial.println(serverDisconnectedStr);
                delay(100); // allow gracefull session close
                
                Serial.println();
                Serial.println();
            } // end: if(gyroData.isValid)
/*
            else
            {
                Serial.println(" gyroData not isValid");
            }
*/
        }
        else
        {
            Serial.println(" ERROR Sender: couldnt connect");
        }
    } // end: if(sendcurrentTime > (lastSendTime+sendDelayBetween) )
    /*
    else    {
        Serial.print("  sendcurrentTime:  "); Serial.println(sendcurrentTime);
        Serial.print("  lastSendTime:     "); Serial.println(lastSendTime);
        Serial.print("  sendDelayBetween: "); Serial.println(sendDelayBetween);
        delay(1000);
    }
    */
} // end: client_SendData
#endif


/* ** defines ** */
// use this?:
//const char HTTP [] PROGMEM = "http:";
//response1 + = FPSTR (HTTP); ``

extern const char* http_Common_OK;// "HTTP/1.1 200 OK\n..."

#endif // MODE_SERVER==false
