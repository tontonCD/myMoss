// file: ESP_WebServer_Server.cpp



#include "ESP_WebServer.h"


// TODO: used twice, move to settings.h or webserver.h ?
#if true
/* if error, 
 *  - set to false and edit these values (ssid/passord), 
 *  OR (better):
 *  - duplicate "credentials.example" as "credentials.h" and edit (look for "How TO") */
# include "credentials.h"
#else
#define ssid_LocalNetwork      "your_Router_ssid"
#define password_LocalNetwork  "your_password_for_Router"
#define ssid_AccessPoint       "define_your_AccessPoint_name_here"
#define password_AccessPoint   "define_your_AccessPoint_password_here"
#endif


// note: Wemo => "D4  IO, 10k pull-up, BUILTIN_LED    GPIO2 "
//  https://escapequotes.net/esp8266-wemos-d1-mini-pins-and-diagram/ ;
//   https://github.com/esp8266/Arduino/blob/2.5.0-beta2/variants/d1/pins_arduino.h#L38-L53





/* ***** UTILITIES ***** */

/** changes "\r" (control character) in "\\" (printable charcter) */
void print_EscapingCRLF(const char *iChars)  {
    const char *p = iChars;
    while(*p > 0)
    {
        if(       *p == '\r')   {
            Serial.print("\\r");   // escaped, prints: "\r";
        } else if(*p == '\n')   {
            Serial.print("\\n");
        } else                  {
            Serial.print(*p);
        }
        p += 1;
    }
}
void print_EscapingCRLF(String iString)  {
    print_EscapingCRLF( (const char *)iString.c_str() );
}
String extractMsg(String wholeMsg)
{
    // check with non-escaped version // Curly_bracket
    int mStart = wholeMsg.indexOf("{");
    int mEnd =   wholeMsg.indexOf("}", mStart);
    //Serial.printf(" mStart: %i\n", mStart);
    //Serial.printf(" mEnd:   %i\n", mEnd);

    if(mStart<0)    {
        // check with escaped version
        mStart = wholeMsg.indexOf("%7B");
        mEnd =   wholeMsg.indexOf("%7D", mStart);
        //Serial.printf(" mStart: %i\n", mStart);
        //Serial.printf(" mEnd:   %i\n", mEnd);
    }
    String result;
    if(mStart<0)
        return result;
    result = wholeMsg.substring(mStart+1, mEnd); // (from, to)
    //Serial.print(" extracted msg:    "); Serial.println(result);
    return result;
 }

 
 /*  param theMsg: can be the whole message, or a message i.e. { (key):(value)[, ...] }
  *  param theKey: can be "", "angle", ...
  */
 String extractWithKey(String theMsg, const char *theKey)
 {
//    Serial.print("  **extractWithKey() ");
    //Serial.print("   theMsg: "); Serial.println(theMsg);
    //Serial.print("   theMsg: "); Serial.println(theMsg.c_str());
//    Serial.print("   theKey: "); Serial.println(theKey);
    
    String valueStr = "(failed)";
    int keyStart =  theMsg.indexOf(theKey); // + strlen("angle")
    if(keyStart < 0)
    {
        valueStr = "(key not found)";
        return valueStr;
    }



/* REF
    int angleIDStart = msg.indexOf("angle"); // + strlen("angle")
    int angleValueStart =     msg.indexOf(":", angleIDStart) + 1;
    int angleValueEnd =   msg.indexOf(",", angleValueStart);
    String angleStr = msg.substring(angleValueStart, angleValueEnd);
 */
    
    //int keyEnd =    keyStart + strlen(theKey); 

    // TODO: use (unsigned char *)?
    int valueStart = theMsg.indexOf(     ":", keyStart) + 1;  // CHANGED: was "="
//    Serial.print("  valueStart: "); Serial.println(valueStart);
    // TEST
    String testStr = theMsg.substring(valueStart, valueStart+3);
//    Serial.print("  value Starts with: \""); Serial.print(testStr); Serial.println("\"");

    /* skip spaces! (for "key: value") */
    while(true)
    {
        if(       theMsg.indexOf(" ",   valueStart) == valueStart)
        {
            valueStart += 1;
            //testStr = theMsg.substring(valueStart, valueStart+3);
            //Serial.print("  value Starts now with: \""); Serial.print(testStr); Serial.println("\"");
        } else if(theMsg.indexOf("%20", valueStart) == valueStart)
        {
            valueStart += 3;
            //testStr = theMsg.substring(valueStart, valueStart+3);
            //Serial.print("  value Starts now with: \""); Serial.print(testStr); Serial.println("\"");
        } else
            break;
    }
    
    int valueEnd =   theMsg.indexOf(         "\n",   valueStart); // TODO: THIS is ONLY for http Header, e.g. "User-Agent: ...\n"

    if(valueEnd<0)  {
        valueEnd =  theMsg.indexOf(         ",",   valueStart);
        //Serial.print("  valueEnd (,): "); Serial.println(valueEnd);
    }
    if(valueEnd<0)  {
        valueEnd =  theMsg.indexOf(          " ",   valueStart);
        //Serial.print("  valueEnd ( ): "); Serial.println(valueEnd);
    }
    if(valueEnd<0)  {
        valueEnd =  theMsg.indexOf(          "}",   valueStart); // TODO: choose: avoid this (message doesn't contain {}) or keep {}
        //Serial.print("  valueEnd (}): "); Serial.println(valueEnd);
    }
    if(valueEnd<0)  {
        valueEnd =  theMsg.indexOf(          "%7D", valueStart); // for escaped("}")
        //Serial.print("  valueEnd (}): "); Serial.println(valueEnd);
    }
    if(valueEnd<0)  {
        valueEnd =  theMsg.length();
        //Serial.print("  valueEnd (theMsg.length()): "); Serial.println(valueEnd);
    }

    if(valueEnd<0)  {
        Serial.println(" extractWithKey() -> ERROR");
    }
    else    
    {
        valueStr = theMsg.substring(valueStart, valueEnd);
        //Serial.print(" found (key:value) => ("); Serial.print(theKey); Serial.print(":"); Serial.print(valueStr.c_str()); Serial.println(")");
    }

    return valueStr;
}

/*
bool extractOne(String theWholeMsg)
{
    Serial.println(" ** calling extractMsg() from extractOne");
    String msg = extractMsg(theWholeMsg);
    Serial.println(" ** ");
    
    // TEMP (test)
    String angleStr = extractWithKey(msg, "angle");
    float angleValue = angleStr.toInt();

    String timeStr = extractWithKey(msg, "time");
    unsigned long timeValue = timeStr.toInt();

    return true;
}
*/

//void printToBoth(char *message, float value)  {   
//}









/* ** defines ** */
// use this?:
//const char HTTP [] PROGMEM = "http:";
//response1 + = FPSTR (HTTP); ``

#define CRLF "\r\n"
//const char CRLF [] PROGMEM = "\r\n";

// SHOULD disapear, @see printlnBoth_httpOK()
const char* http_Common_OK = // renamed from http_OK
"HTTP/1.1 200 OK"CRLF
"Content-type:text/html\r\n"
"Connection: close\r\n\r\n";

// TEST: with param (if ok, move)
const char* http_Server_OK_1 = // part 1
"HTTP/1.1 200 OK\r\n"
"Content-type:text/html\r\n";
const char* http_Server_OK_2 = // part 2
"Connection: close\r\n\r\n"; // TODO: the second "\r\n" is embed (dont use println() after), check and report on http_Common_OK

/*
const char* html_Server_TOP = // renamed from html_TOP
"<!DOCTYPE html><html>\n"
"<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
"<link rel=\"icon\" href=\"data:,\">\n";



const char* html_Server_Heading = "<body><h1>ESP32 Web Server</h1>"; // renamed from html_Heading

// Client state - actually unused, @see "/data"
const char* html_Server_DHT = 
"<table>\n"
"	<tr><td style=\"width:150px\">Humidity:</td> <td>%.2f %%    </td> <td>&nbsp;     </td> </tr>\n"
"	<tr><td>          Temperature:         </td> <td>%.2f &deg;C</td> <td>%.2f &deg;F</td> </tr>\n"
"   <tr><td>          Heat index:          </td> <td>%.2f &deg;C</td> <td>%.2f &deg;F</td> </tr>\n"
"</table>\n";


// Client state - actually unused, @see "/data"
const char* html_Server_GYRO =  // gyroData.ax, gyroData.ay, gyroData.az, gyroData.temperature; gyroData.tilt
"<table>\n"
"    <tr><td style=\"width:150px\">ax:</td> <td>%.2f       </td> </tr>\n"
"   <tr><td>                       ay:</td> <td>%.2f       </td> </tr>\n"
"   <tr><td>                       az:</td> <td>%.2f       </td> </tr>\n"
"   <tr><td>              temperature:</td> <td>%.2f &deg;C</td> </tr>\n"
"</table>\n";

const char* html_Server_ServerSetting =
"<div>"
"<strong><em>Server Settings</em></strong><br/>"
"<table>"
"<tr><td>builtIn led (test)</td><td><a class=\"linkb %s\" href=\"/bion\">ON</a> <a class=\"linkb %s\" href=\"/bioff\">OFF</a></td></tr>"
"</table></div><br>";





const char* html_Server_END = 
"</body></html>\n"
// The HTTP response ends with another blank line:
"\n";
*/
