// file: ESP_WebServer_Server.cpp


#include "settings.h"

#if MODE_SERVER

#include "ESP_WebServer.h"

// DISPLACED -> "settings.h"
#if USE_DHT_SENSOR
# include "dhtReader.h"
  DHTReader dhtReader; // TODO: change to clientDhtReader/serverDhtReader, risks of conflicts
  DHTData dhtData;
/*
//#   include "DHT.h"
// Please define the Digital pin connected to the DHT sensor:
#   if   defined(ESP32  )   // <- the Board is (e.g.): "ESP32 Dev Module"
const uint8_t DHTPIN = 2;       
#   elif defined(ESP8266)   // <- the Board is (e.g.): "LOLIN(WEMO) D1 R2 & mini" 
const uint8_t DHTPIN = D2; // for D2, but D2 undefined here, consult datasheet     
#   endif
const uint8_t DHTTYPE = DHT22; // real type can be: "DHT 22", "AM2302", "AM2321"
*/
#elif USE_DHT_READER
#endif // USE_DHT


#if USE_GYRO_SENSOR
# include "gyroReader.h"
  GYROReader serverGyroReader; // was: gyroReader
  GYROData gyroData;

// DISPLACED
/*
  const uint8_t SDA_PIN = D3;
  const uint8_t SDC_PIN = D4;
*/  
#elif USE_GYRO_READER
//# include "gyroReader.h"
//  GYROData gyroData;
#endif // USE_GYRO
// todo: displace to ".h"?


#if true
/* if error, 
 *  - set to false and edit these values (ssid/passord), 
 *  OR (better):
 *  - duplicate "credentials.example" as "credentials.h" and edit (look for "How TO") */
# include "credentials.h"
#else
#define ssid_LocalNetwork      "your_Router_ssid"
#define password_LocalNetwork  "your_password_for_Router"
#define ssid_AccessPoint       "define_your_AccessPoint_name_here"
#define password_AccessPoint   "define_your_AccessPoint_password_here"
#endif

#if USE_BUILT_IN_LED 
bool serverBuiltinLedState; 
int  serverBuiltinLedPin;   // pin number
int serverLedHIGH; // maxValue, replaces: HIGH
int serverLedLOW;  // minValue, replaces: LOW; not usefull but to simplify searchin occurences 
// PB: with 255/0 and analogWrite, doesn't work !
#endif

Server_Received server_Received; // replaces serverData; TODO: clean serverData
// added: in response to a Web Client (e.g. "/cbi:" or "/sbi:"), set the data to reply when a myMoss Client connects.
Server_MessageSent server_Sent;



// TEST // {angle=%f, time=%u}
MessageKey aKey = mkLedStatus;
OneMessage aMessage; 



/* Set web server port number, init to 80 */
WiFiServer server(80); // TODO: defined in _common

WiFiClient server_available()	{
	return server.available();
}




void beginServer_Thru_LocalNetwork();
void beginServer_As_AccessPoint();
void beginWifi()  {

#if USE_BUILT_IN_LED 
    pinMode(serverBuiltinLedPin, OUTPUT);

    // Set outputs to LOW
    digitalWrite(serverBuiltinLedPin, serverLedLOW);
    serverBuiltinLedState = true;
#endif

    /* init SENSORS */
#if USE_DHT_SENSOR
    Serial.println("Using: DHT Sensor");
    dhtReader.beginReader(DHTPIN, DHTTYPE);
#endif

#if USE_GYRO_SENSOR
    Serial.println("Using: GYRO Sensor");
    serverGyroReader.initReader(SDA_PIN, SDC_PIN);
#endif

    server_Sent.smsReceived = true; // reset, don't send anything
    
#if MODE_SAP    //  USE__SET_SOFT_AP
    beginServer_As_AccessPoint();
#else           // USE__LOCAL_NETWORK
    beginServer_Thru_LocalNetwork();
#endif
}


#if MODE_SAP==false
void beginServer_Thru_LocalNetwork()
{
    Serial.println("Connecting to Local Network");
    
    // set mode to: WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF
    WiFi.mode(WIFI_STA); // WeMo only ?
    
    // Connect to Wi-Fi network with SSID and password
    Serial.printf(" Connecting to %s", ssid_LocalNetwork);

    WiFi.begin(ssid_LocalNetwork, password_LocalNetwork);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    } 
    Serial.println();
    if(WiFi.status() != WL_CONNECTED)
    {
        Serial.println(" Failed to connect");
        return;
    }
    
    // Print local IP address and start web server
    Serial.println(" WiFi connected.");
    Serial.print(" IP address: ");Serial.println(WiFi.localIP());
    
    server.begin();
    Serial.println(" HTTP server started");
}
#endif // MODE_SAP==false

#if MODE_SAP
void beginServer_As_AccessPoint()
{
    Serial.println("Connecting as Access Point");
    
    // Connect to Wi-Fi network with SSID and password

// change IP Adress (only ESP8286?)
// fails; keep and see...
# if false 
    // https://github.com/esp8266/Arduino/issues/2208
    IPAddress local_IP(192,168,4,22);
    IPAddress gateway(192,168,4,9);
    IPAddress subnet(255,255,255,0);
    Serial.print("Changing IP to: ");Serial.println(local_IP);

    // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-class.html
    // channel - optional parameter to set Wi-Fi channel, from 1 to 13. Default channel = 1
    WiFi.softAPConfig(local_IP, gateway, subnet); // 
    //if (!WiFi.softAP( ssid, password,channel,0))
    if (!WiFi.softAP( ssid_AccessPoint, password_AccessPoint ))
    {
        Serial.println("WiFi.softAP failed.(Password too short?)");
        return;
    }

  
    //IPAddress myIP = WiFi.softAPIP();
    IPAddress myIP = WiFi.softAPIP();
    //Serial.println();
    //Serial.print("AP IP address: ");
    //Serial.println(myIP);

  // doesn work; @see https://esp32.com/viewtopic.php?t=13371
# else
    // Remove the password parameter, if you want the AP (Access Point) to be open
    WiFi.softAP(ssid_AccessPoint, password_AccessPoint);
    IPAddress myIP = WiFi.softAPIP();
# endif

    /* MAC adress */
    uint8_t macAddr[6]; // 24:62:AB:DC:6E:A0 ?
    WiFi.softAPmacAddress(macAddr);
    Serial.printf("MAC address = %02x:%02x:%02x:%02x:%02x:%02x\n", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
    Serial.printf("MAC address = %s\n", WiFi.softAPmacAddress().c_str());
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());

    /* IP */
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    /* in this mode the IP is always 192.168.4.1 (?) */
    
    server.begin();
	Serial.println("Access Point started");
}
#endif // MODE_SAP




/* ***** UTILITIES ***** */
#if MODE_SERVER
/* server only */

// OBSO: now in _Common
/*
String extractMsg(String wholeMsg)
{
    // check with non-escaped version // Curly_bracket
    int mStart = wholeMsg.indexOf("{");
    int mEnd =   wholeMsg.indexOf("}", mStart);
    Serial.printf(" mStart: %i\n", mStart);
    Serial.printf(" mEnd:   %i\n", mEnd);

    if(mStart<0)    {
        // check with escaped version
        mStart = wholeMsg.indexOf("%7B");
        mEnd =   wholeMsg.indexOf("%7D", mStart);
        Serial.printf(" mStart: %i\n", mStart);
        Serial.printf(" mEnd:   %i\n", mEnd);
    }
    String result;
    if(mStart<0)
        return result;
    result = wholeMsg.substring(mStart+1, mEnd); // (from, to)
    Serial.print(" extracted msg:    "); Serial.println(result);
    return result;
 }
*/
 
 /*  param theMsg: can be the whole message, or a message i.e. { (key):(value)[, ...] }
  *  param theKey: can be "", "angle", ...
  */
 /*

bool extractOne(String theWholeMsg)
{
    Serial.println(" ** calling extractMsg() from extractOne");
    String msg = extractMsg(theWholeMsg);
    Serial.println(" ** ");
    
    // TEMP (test)
    String angleStr = extractWithKey(msg, "angle");
    float angleValue = angleStr.toInt();

    String timeStr = extractWithKey(msg, "time");
    unsigned long timeValue = timeStr.toInt();

    return true;
}
*/



//void printToBoth(char *message, float value)  {  
//}



WiFiClient wifiIncomingClient;
String incommingClientHttpHeader = "";  // renamed, was: httpClientHeader
String httpHeaderToSend;                // replaces: builtMessage
/**  */

void printBoth(const char *iString)   {
    Serial.print(    iString ); 
    wifiIncomingClient.print(iString);
}
void printBoth(uint32_t iValue)   {
    Serial.print(    iValue ); 
    wifiIncomingClient.print(iValue);
}
void printlnBoth()   {
    Serial.print("\\r\\n"); // double slash for it prints as text
    wifiIncomingClient.print("\r\n"); // FIX??? (replaces println())
}

// TODO: useless now, restore occurences
void printlnBoth_httpOK(String message) {
/*
const char* http_Common_OK = // renamed from http_OK
"HTTP/1.1 200 OK\r\n"
"Content-type:text/html\r\n"
"Connection: close\r\n";

wifiIncomingClient.println(http_Common_OK);
*/

#if false
    printBoth("HTTP/1.1 200 OK");        printlnBoth();
    printBoth("Content-type:text/html"); printlnBoth();
    printBoth("Connection: close");      printlnBoth();
    printlnBoth();
#else
    char *part = "HTTP/1.1 200 OK"; 
    message += part; message += "\r\n";  
    Serial.print(part); Serial.print("\\r\\n"); 
    
    part = "Content-type:text/html";
    message += part; message += "\r\n";  
    Serial.print(part); Serial.print("\\r\\n"); 

    part = "Connection: close";
    message += part; message += "\r\n";  
    Serial.print(part); Serial.print("\\r\\n"); 

    message += "\r\n"; 
    Serial.print("\\r\\n"); 

    Serial.println(); 
    wifiIncomingClient.print(message);
#endif

    
}

void _sendHttpHeader()    {
    // before server_respondTo_data or server_displayPage, 
    // send the header first
    if(httpHeaderToSend.length() > 0)   { // if not sent
        wifiIncomingClient.print(httpHeaderToSend);
        Serial.print("Responding: "); print_EscapingCRLF(httpHeaderToSend); Serial.println();
        // don't sen it later
        httpHeaderToSend = "";
    }
    // this because, in *response* to a client we need to send data "inside" the header, in all other cases after it
}

void server_respondTo_data()  {

    //String result;
    
    //wifiIncomingClient.print( "{ "); 


/* REFACTORED (displaced)
#if USE_DHT_SENSOR
    if(dhtData.isValid)
        wifiIncomingClient.printf("%.2f\t%.2f", dhtData.h, dhtData.t);
    else
        wifiIncomingClient.printf("nc");
    // TODO: refactor
#endif

#if USE_GYRO_READER 

    Serial.println(" sending gyro");
    // NEEDS: update(?)
    
//    wifiIncomingClient.printf("%.2f\t%.2f<br>\r\n", gyroData.tilt, gyroData.temperature);
    // TODO: clean gyroData

    //Serial.print(    " saved angle: "); Serial.println(  mem_angle ); // gyroDataSync.gdsAngle);
    //Serial.print(    " saved angle: "); Serial.println(  mem_angle ); // gyroDataSync.gdsAngle);
#endif

#if USE_GYRO_READER 
    Serial.print( " saved angle (client)    : ");  Serial.println( server_Received.smrAngleValue ); 
    Serial.print( " saved time (client)     : ");  Serial.println( server_Received.smrMesureTime );
    Serial.print( " saved SleepTime (client):  "); Serial.println( server_Received.smrSleepTime );
# if USE_BUILT_IN_LED 
    Serial.print( " saved LedStatus (client):  "); Serial.println( server_Received.smrLedStatus );
# endif
?
#endif
*/
    _sendHttpHeader();


#if false // OLD
    wifiIncomingClient.print( myMossIDKey": ");   wifiIncomingClient.print( server_Received.smrMyMossID );   wifiIncomingClient.print(", ");
    wifiIncomingClient.print( "angle: ");      wifiIncomingClient.print( server_Received.smrAngleValue ); wifiIncomingClient.print(", ");
    wifiIncomingClient.print( "time: ");       wifiIncomingClient.print( server_Received.smrMesureTime ); wifiIncomingClient.print(", ");
    wifiIncomingClient.print( deepSleepTimeKey": ");  wifiIncomingClient.print( server_Received.smrSleepTime  ); wifiIncomingClient.print(", ");
# if USE_BUILT_IN_LED 
    wifiIncomingClient.print( "ledStatus: ");  wifiIncomingClient.print( server_Received.smrLedStatus  );     
# endif
#else
    String messageToSend = "{ "; 
    
    messageToSend += myMossIDKey": ";      // "myMossID"
    messageToSend += server_Received.smrMyMossID;
    messageToSend += ", ";

    messageToSend += angleValueKey": ";    // "angle"
    messageToSend += server_Received.smrAngleValue;
    messageToSend += ", ";

    messageToSend += mesureTimeKey": ";    // "time"
    messageToSend += server_Received.smrMesureTime;
    messageToSend += ", ";

    messageToSend += deepSleepTimeKey": "; // "sleepTime"
    messageToSend += server_Received.smrSleepTime;
# if USE_BUILT_IN_LED 
    messageToSend += ", ";

    messageToSend += ledStatusKey": ";     // "ledStatus"
    messageToSend += server_Received.smrLedStatus;
# endif
#endif

    messageToSend += " }";

    Serial.print("Sending: "); print_EscapingCRLF(messageToSend); Serial.println();
    wifiIncomingClient.println(messageToSend); // ! needs the ln (the last crlf)

    server_Received.smrTransmit = true;


//     wifiIncomingClient.print( " }\r\n"); 
    //wifiIncomingClient.println("<br>");
} // server_respondTo_data()

void server_displayPage()  { // in response to all request (but not "/Get setData")

    _sendHttpHeader();
                            
    Serial.print("Sending: "); Serial.print(html_Server_TOP); Serial.println("...");

    // Display the HTML web page
    wifiIncomingClient.println(html_Server_TOP);

    // CSS to style the on/off buttons 
    wifiIncomingClient.println(html_Server_CSS);

    // Web Page Heading
    wifiIncomingClient.println(html_Server_Heading);


#if USE_DHT_SENSOR
    // transmit the data we received from Client
    if(dhtData.isValid)
        wifiIncomingClient.printf(html_Server_DHT,  dhtData.h, dhtData.t, dhtData.f, dhtData.hic, dhtData.hif);
    else
        wifiIncomingClient.println("Failed to read from DHT sensor");
    // TODO: refactor
#endif


#if USE_GYRO_SENSOR
    if(gyroData.isValid)
        wifiIncomingClient.printf(html_Server_GYRO,  gyroData.ax, gyroData.ay, gyroData.az, gyroData.temperature);
    else
        wifiIncomingClient.println("Failed to read from GYRO sensor");
#endif

/*
    // TRACE
    wifiIncomingClient.printf("servCurrentTime      : %ld<br>", servCurrentTime);
    wifiIncomingClient.printf("lastSendTime     : %ld<br>", lastSendTime);
    wifiIncomingClient.printf("sendDelayBetween : %ld<br>\n", sendDelayBetween);
*/

#if USE_BUILT_IN_LED 
    // state for Server
    if(serverBuiltinLedState)
    {
        wifiIncomingClient.printf(html_Server_ServerSetting, "On", "Off"); wifiIncomingClient.println();
    }
    else
    {
        wifiIncomingClient.printf(html_Server_ServerSetting, "Off", "On"); wifiIncomingClient.println();
    }
#else
    wifiIncomingClient.print(html_Server_ServerSetting); wifiIncomingClient.println();
#endif

    // state for Client
#if USE_BUILT_IN_LED 
    if(server_Received.smrLedStatus)
    {
        wifiIncomingClient.printf(html_Server_ClientSetting_1, "On", "Off"); wifiIncomingClient.println();
    }
    else
    {
        wifiIncomingClient.printf(html_Server_ClientSetting_1, "Off", "On"); wifiIncomingClient.println();
    }
#else
    wifiIncomingClient.print(html_Server_ClientSetting_1); wifiIncomingClient.println();
#endif

    switch(server_Received.smrSleepTime)
    {
        case 0:  wifiIncomingClient.printf(html_Server_ClientSetting_2, "On",  "Off", "Off", "Off", "Off"); break;
        case 1:  wifiIncomingClient.printf(html_Server_ClientSetting_2, "Off", "On",  "Off", "Off", "Off"); break;
        case 15: wifiIncomingClient.printf(html_Server_ClientSetting_2, "Off", "Off", "On",  "Off", "Off"); break;
        case 30: wifiIncomingClient.printf(html_Server_ClientSetting_2, "Off", "Off", "Off", "On",  "Off"); break;
        case 60: wifiIncomingClient.printf(html_Server_ClientSetting_2, "Off", "Off", "Off", "Off", "On"); break;
        default: wifiIncomingClient.printf(html_Server_ClientSetting_2, "Off", "Off", "Off", "Off", "Off"); // break;
    }
    wifiIncomingClient.println();


    wifiIncomingClient.println(html_Server_END);
} // server_displayPage()

#if USE_BUILT_IN_LED
void server_respondTo_cbi(int value)  { // buildinLed -> On
    Serial.print("  -> Client builtinLed: "); Serial.println(value);

    // Save data for when the Client connects:
    server_Sent.smsLedStatus = value;     
    server_Sent.smsLedStatusShouldSend = true;
    server_Sent.smsReceived = false;
    
    server_displayPage();
} // server_respondTo_bion()
#endif

#if USE_BUILT_IN_LED 
void server_respondTo_sbi(int value)  { // buildinLed -> Off
    Serial.print("  -> Server builtinLed: "); Serial.println(value);
    // test
    //int value = analogRead(serverBuiltinLedPin);
    //Serial.print(" current value:"); Serial.println(value);

    if(value > 0)
    {
        serverBuiltinLedState = true;
        digitalWrite(serverBuiltinLedPin, serverLedLOW);    // why? but it's the right command...
    } else
    {
        serverBuiltinLedState = false;
        digitalWrite(serverBuiltinLedPin, serverLedHIGH);   // why? (ditto)    
    }
    

    // test
    //value = analogRead(serverBuiltinLedPin);
    //Serial.print(" current value:"); Serial.println(value);

    server_displayPage();
} // server_respondTo_bioff()
#endif 

void server_respondTo_sleepTime(unsigned long iValue)  { // buildinLed -> Off
    Serial.print(" -> set Sleep Time: ");Serial.println(iValue);

    // Save data for when the Client connects:
    server_Sent.smsSleepTime = iValue;     
    server_Sent.smsSleepTimeShouldSend = true;
    server_Sent.smsReceived = false;
    
    server_displayPage();
} // server_respondTo_sleepTime()

void server_respondTo_calibration()  { // buildinLed -> Off
    Serial.println(" -> do Calibration");

    // Save data for when the Client connects: 
    server_Sent.smsDoCalibrationShouldSend = true;
    server_Sent.smsReceived = false;

    server_displayPage();
} // server_respondTo_doCalibration()


void server_respondTo_setData()  { // "/Get setData ..."
/* request is (e.g.): /setData {angle=10.2, time=5}
 *  if http navigator => "GET /setData%20%7Bangle=10.2,%20time=5%7D HTTP/1.1\r\n"
 *  if myMoss Client  => "GET /setData {angle=69.396271, time=523127}\r\n"
 *  
 *  planed improvment, // TODO: can loop?, e.g. "/setData {angle=10.2, time=5}{angle=12.2, time=6}" and more
 *
 * CONSTRAINTS: 
 *  {}  around          bracket, for each element
 *  identifier:value    inside; (spaces before/in/at end: ???)
 *  ',' or '}' or ' '   at end;
 *  case sensitive, ""sleepTime" not the same than "sleeptime"
 *  @see parser
 */

    //Serial.println(" ** calling extractMsg()");
    String msg = extractMsg(incommingClientHttpHeader);


    String myMossIDStr =  extractWithKey(msg, myMossIDKey);     // "myMossID"
    String angleStr =     extractWithKey(msg, angleValueKey);   // "angle"
    String timeStr =      extractWithKey(msg, mesureTimeKey); // "time");
    String sleepTimeStr = extractWithKey(msg, deepSleepTimeKey);    // "sleepTime"
#if USE_BUILT_IN_LED 
    String ledStatusStr = extractWithKey(msg, "ledStatus");
#endif
    
    //unsigned long tTimeValue = tTimeStr.toInt();
    
    Serial.print(" "myMossIDKey":  "); Serial.println(myMossIDStr);
    Serial.print(" angle:     "); Serial.println(angleStr);
    Serial.print(" time:      "); Serial.println(timeStr);
    Serial.print(" "deepSleepTimeKey": "); Serial.println(sleepTimeStr);
#if USE_BUILT_IN_LED 
    Serial.print(" ledStatus: "); Serial.println(ledStatusStr);
#endif


    // TODO: 1) embed,
    server_Received.smrMyMossID = myMossIDStr.toInt();
// should use:
//#   if USE_GYRO_READER
    server_Received.smrAngleValue = angleStr.toFloat();;
//#   endif
    
    server_Received.smrMesureTime = timeStr.toInt();

    server_Received.smrSleepTime = sleepTimeStr.toInt();
#if USE_BUILT_IN_LED
    server_Received.smrLedStatus = (ledStatusStr.toInt() > 0); // represents: clientBuiltinLedState
#endif

    // !: don't call server_displayPage();
    // because it's reserved to myMoss Clients

    // now, because will be followed with httpOk
    //Serial.print("Sending to Client: ");
    
    // Check with all response to "/setData":
    bool doSend = (server_Sent.smsReceived == false);
    httpHeaderToSend = http_Server_OK_1; // was: messageToSend
    if(doSend)
    {
        bool addComma = false;
        httpHeaderToSend += "data: { "; // note: the httpHeaderToSend will be * data inserted* in the normal http_Server_OK response!
        
        if(server_Sent.smsSleepTimeShouldSend)  {
            httpHeaderToSend += deepSleepTimeKey": ";
            httpHeaderToSend += server_Sent.smsSleepTime;
            server_Sent.smsSleepTimeShouldSend = false;
            addComma = true;
        }
            
#if USE_BUILT_IN_LED 
        if(server_Sent.smsLedStatusShouldSend)  {
            if(addComma)
                httpHeaderToSend += ", ";
            httpHeaderToSend += "ledStatus: ";
            httpHeaderToSend += server_Sent.smsLedStatus;
            server_Sent.smsLedStatusShouldSend = false;
            addComma = true;
        }
#endif
            
        if(server_Sent.smsDoCalibrationShouldSend)  {
            if(addComma)
                httpHeaderToSend += ", ";
            httpHeaderToSend += "calibration: "; httpHeaderToSend += "true";
            server_Sent.smsDoCalibrationShouldSend = false;
            addComma = true;
        }
        httpHeaderToSend += " }\r\n";

 //       Serial.print(messageToSend);
 //       wifiIncomingClient.print(messageToSend);
        server_Sent.smsReceived = true;
        // TODO: should try to connect ? the Client is on line, we have a few seconds
    } // end: if(doSend)

    httpHeaderToSend += http_Server_OK_2;
    //return messageToSend; // may use a global, i.e. builtMessage (@see "builtMessage =")
} // server_respondTo_setData()





#endif // MODE_SERVER

#if MODE_SERVER
/* ***** SERVER ***** */
unsigned long servCurrentTime;
unsigned long servPreviousTime;
unsigned long servTimetout = 2000; 

const char *newClientStr =          "*** New Client: *******";  // for log
const char *clientDisconnectedStr = "Client disconnected ***";  // for log

void server_checkClient()
{
    /* WiFiClient */ wifiIncomingClient = server_available();   // Listen for incoming wifiClients

    
    if (wifiIncomingClient) {                             // If a new client connects,

# if USE_DHT_SENSOR
        dhtData = dhtReader.readDHT();
# endif

# if USE_GYRO_SENSOR
        GYROData gyroData = serverGyroReader.readGYRO();
# endif

        servCurrentTime = millis();
        servPreviousTime = servCurrentTime;
        Serial.println(newClientStr);          // print a message out in the serial port
        
        Serial.println("Client request:");  
        Serial.print("  ");  
        String currentLine = "";                // make a String to hold incoming data from the client

        httpHeaderToSend = http_Common_OK;
        while (wifiIncomingClient.connected() && servCurrentTime - servPreviousTime <= servTimetout) {  // loop while the client's connected
            servCurrentTime = millis();
            if (wifiIncomingClient.available()) {             // if there's bytes to read from the client,
                char c = wifiIncomingClient.read();             // read a byte, then
                //Serial.write(c); // displaced                   // print it out the serial monitor
                incommingClientHttpHeader += c; 
                if (c == '\n') {                    // if the byte is a newline character

                    // TODO: refactor as "parse()"

                    //incommingClientHttpHeader += "\\n";
                    Serial.write("\\n");
                    
                    // END OF LINE
                    
                    // if the current line is blank, you got two newline characters in a row.
                    // that's the end of the client HTTP request, so send a response:
                    if (currentLine.length() == 0) {

                        // END OF MESSAGE
                        
                        Serial.println(); // identation for the log

 /* displaced                        
                        // respond "OK":
                        //Serial.print( "responding: ");Serial.print(http_Common_OK);Serial.println();
                        wifiIncomingClient.println(http_Common_OK); // "HTTP/1.1 200 OK"
*/

                        
                        String usedAgentStr = extractWithKey(incommingClientHttpHeader, "User-Agent"); // unused
                        Serial.print("User-Agent: \""); Serial.print(usedAgentStr); Serial.println("\"");



                        String cmdValue;
#if USE_BUILT_IN_LED 
                        // builtIn:
                        if (     incommingClientHttpHeader.indexOf("GET /cbi") >= 0) {  // was: "bion"/"bioff"
                            cmdValue = extractWithKey(incommingClientHttpHeader, "cbi");
                            server_respondTo_cbi(cmdValue.toInt()); // was: server_respondTo_bion()
                        } 
                        else if (incommingClientHttpHeader.indexOf("GET /sbi") >= 0) {
                            cmdValue = extractWithKey(incommingClientHttpHeader, "sbi");
                            server_respondTo_sbi(cmdValue.toInt());
                        }
                        else
#endif
                        if (incommingClientHttpHeader.indexOf("GET /"deepSleepTimeKey) >= 0) {
                            String dstStr = extractWithKey(incommingClientHttpHeader, deepSleepTimeKey);
                            Serial.print(" request for sleepTime:  "); Serial.println(dstStr);
                            server_respondTo_sleepTime(dstStr.toInt());
                        }
                        else if (incommingClientHttpHeader.indexOf("GET /calibration") >= 0) {
                            server_respondTo_calibration();
                        }
                        else if (incommingClientHttpHeader.indexOf("GET /data") >= 0)  {
                            server_respondTo_data();
                            //break;
                        }
                        
                        else if (incommingClientHttpHeader.indexOf("GET /setData") >= 0)  {
                            /* httpHeaderToSend = */ server_respondTo_setData();
                        } // end: if (incommingClientHttpHeader.indexOf("GET /setData") >= 0)

                        else
                        {
                            // Display the HTML web page
                            // note that the httpHeader will be send before HTML (insinde server_displayPage())
                            server_displayPage();
                        }

                        // respond "OK":
                        //  critical: a)if preparing a Web Page send the httpHeader before, if not the response appears in the Web Page!!
                        //            b) consider that in response to myMoss, all following the header is lost, so put all data in in the httpHeader
//#if false
//                        Serial.println();Serial.print(http_Common_OK);Serial.println();
//                        wifiIncomingClient.println(http_Common_OK); // "HTTP/1.1 200 OK"..."Connection: close\r\n";
//#elif false // new (better)
//                        printlnBoth_httpOK(httpHeaderToSend);
//                        Serial.println();
//#else // ok

                        // httpHeaderToSend is: (http_Server_OK),
                        // or:                  (http_Server_OK_1 + {data:xx} + http_Server_OK_2)
                        //if(builtMessage.length() == 0)
                        //{
                        //    builtMessage = http_Common_OK;
                        //}

#if false // OK but refactored
                        if(httpHeaderToSend.length() > 0)   { // if not sent
                            wifiIncomingClient.print(httpHeaderToSend); 
                            Serial.print("Responding: "); print_EscapingCRLF(httpHeaderToSend); Serial.println();
#else
                        // send httpHeader, if it was not
                        _sendHttpHeader();
#endif
                        

//#endif // #if false
                            
                        // Break out of the while loop
                        break;
                    } // end: if(currentLine.length() == 0)
                    
                    else { // if you got a newline, then clear currentLine
                        currentLine = "";
                        //Serial.print(" "); // identation for the log
                    }
                } // end:  if (c == '\n')
                else  { 
                    //incommingClientHttpHeader += c;
                    Serial.write(c);
                    if (c != '\r') {  // if you got anything else but a carriage return character,
                        currentLine += c;      // add it to the end of the currentLine
                    }
                }
            } // if available()
        } // while
    
        // Clear the header variable
        incommingClientHttpHeader = "";
        // Close the connection
        wifiIncomingClient.stop();
        Serial.println(clientDisconnectedStr);
        Serial.println("");
    
        //return;
    } //  if (wifiIncomingClient)
}
//#else // MODE_SERVER -> false
//void client_SendData()
//{
//} // end: client_SendData
#endif // MODE_SERVER

/* ** defines ** */
// use this?:
//const char HTTP [] PROGMEM = "http:";
//response1 + = FPSTR (HTTP); ``





const char* html_Server_TOP = // renamed from html_TOP
"<!DOCTYPE html><html>\r\n" 
"<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n"
"<link rel=\"icon\" href=\"data:,\">\r\n";

const char* html_Server_CSS = // renamed from html_CSS
"<style>\r\n"
"html, table { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\r\n"
".linkb { display: inline-block; padding: 5px 10px; text-align: center; text-decoration: none;"
" color: #ffffff; background-color: #7aa8b7; border-radius: 6px; outline: none; }"
".On  { background-color: #4CAF50; }"
".Off { background-color: #555555; }"
"td { width: 200px; border-style: groove; }"
"</style></style></head>\r\n";

const char* html_Server_Heading = "<body><h1>ESP32 Web Server</h1>"; // renamed from html_Heading

// Client state - actually unused, @see "/data"
const char* html_Server_DHT = 
"<table>\r\n"
"	<tr><td style=\"width:150px\">Humidity:</td> <td>%.2f %%    </td> <td>&nbsp;     </td> </tr>\r\n"
"	<tr><td>          Temperature:         </td> <td>%.2f &deg;C</td> <td>%.2f &deg;F</td> </tr>\r\n"
"   <tr><td>          Heat index:          </td> <td>%.2f &deg;C</td> <td>%.2f &deg;F</td> </tr>\r\n"
"</table>\r\n";


// Client state - actually unused, @see "/data"
const char* html_Server_GYRO =  // gyroData.ax, gyroData.ay, gyroData.az, gyroData.temperature; gyroData.tilt
"<table>\r\n"
"    <tr><td style=\"width:150px\">ax:</td> <td>%.2f       </td> </tr>\r\n"
"   <tr><td>                       ay:</td> <td>%.2f       </td> </tr>\r\n"
"   <tr><td>                       az:</td> <td>%.2f       </td> </tr>\r\n"
"   <tr><td>              temperature:</td> <td>%.2f &deg;C</td> </tr>\r\n"
"</table>\r\n";

const char* html_Server_ServerSetting = // params: On/Off, On/Off
"<strong><em>Server Settings</em></strong><br/>"
"<table>"
"<tr><td>builtIn led (test)</td><td>"
#if USE_BUILT_IN_LED 
"<a class=\"linkb %s\" href=\"/sbi:1\">ON</a> <a class=\"linkb %s\" href=\"/sbi:0\">OFF</a>"// was "bion"/"bioff"
#else
"<em>not available</em>"
#endif
"</td></tr></table></div><br>";


const char* html_Server_ClientSetting_1 = // params x2: On/Off, On/Off
"<div>"
"<strong><em>Client Settings</em></strong><br/>"
"<table>"
"<tr><td>builtIn led (test)</td><td>"
#if USE_BUILT_IN_LED 
"<a class=\"linkb %s\" href=\"/cbi:1\">ON</a> <a class=\"linkb %s\" href=\"/cbi:0\">OFF</a>"
#else
"<em>not available</em>"
#endif
"</td></tr>"
"<tr><td colspan=\"2\"><a class=\"linkb\" href=\"/calibration\">Proceed to Calibration</a></td></tr>";

const char* html_Server_ClientSetting_2 = // params x5, e.g. : On, Off, Off, Off, Off
"<tr><td>DeepSleep Time (0=off)</td> <td>"
"<a class=\"linkb %s\" href=\"/sleepTime: 0\"> 0</a> " // %s: On or Off
"<a class=\"linkb %s\" href=\"/sleepTime: 1\"> 1</a> "
"<a class=\"linkb %s\" href=\"/sleepTime:15\">15</a> "
"<a class=\"linkb %s\" href=\"/sleepTime:30\">30</a> "
"<a class=\"linkb %s\" href=\"/sleepTime:60\">60</a></td></tr>"
"</table></div>";




const char* html_Server_END = 
"</body></html>";


#endif // MODE_SERVER
