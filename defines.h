// file: defines.h

/** 
 *  pre-defined modes, because (for example, ):
 *  "Server with DeepSleep" or "Client as SAP" doesn't make sense!
 */
#if false
typedef enum {
    modeUndef = 0,
    /** acts as a server, connecting to a Router; clients clients must connect to a Router, then send request to obtain data */
    modeServer_viaRouter     = 1,
    /** acts as a server, connecting as a Soft Access Point; clients must connect to it (not to a router), then send request to obtain data */
    modeServer_asSap         = 2,
    /** acts as a client (sending data to a server), with no DeepSleep feature             */
    modeClient_NoDeepSleep   = 3, // for debug
    /** acts as a client (sending data to a server), with DeepSleepFeature                 */
    modeClient_withDeepSleep = 4
} Mode;
#else
# define modeUndef 0
    /** acts as a server, connecting to a Router; clients clients must connect to a Router, then send request to obtain data */
# define modeServer_viaRouter     1
    /** acts as a server, connecting as a Soft Access Point; clients must connect to it (not to a router), then send request to obtain data */
# define modeServer_asSap         2
    /** acts as a client (sending data to a server), with no DeepSleep feature             */
# define modeClient_NoDeepSleep   3 // for debug
    /** acts as a client (sending data to a server), with DeepSleepFeature                 */
# define modeClient_withDeepSleep 4
#endif
/* mySpindle => 2 x modules:
 *  module1, server: modeServer_asSap
 *  module2, sensor: modeClient_NoDeepSleep (if DEBUG) or modeClient_withDeepSleep
 *      the DeepSleep MODE will be set from the Configuration interface
 *      // TODO: manade double_reset as we may want to comeback to no_deepseep
 */

/*
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
*/





 
