// file: dhtReader.cpp



#include "dhtReader.h"
// TODO: TEMP
#include "credentials.h"

#if USE_DHT_SENSOR

DHTData DHTReader::readDHT()
{
    DHTData result;
       // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    result.h = _dht->readHumidity();
    // Read temperature as Celsius (the default)
    result.t = _dht->readTemperature();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    result.f = _dht->readTemperature(true);

    // Check if any reads failed and exit early (to try again).
    if (isnan(result.h) || isnan(result.t) || isnan(result.f)) {
        //Serial.println(F("Failed to read from DHT sensor!"));
        result.isValid = false;
        return result;
    }

    // Compute heat index in Fahrenheit (the default)
    result.hif = _dht->computeHeatIndex(result.f, result.h);
    // Compute heat index in Celsius (isFahreheit = false)
    result.hic = _dht->computeHeatIndex(result.t, result.h, false);

    result.isValid = true;
    return result;
}

#endif // USE_DHT_SENSOR
