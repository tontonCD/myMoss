// file: dhtReader.h

#include "settings.h"
// TODO: TEMP
#include "credentials.h"


#if USE_DHT_SENSOR
// TODO: add?: || USE_DHT_READER

#include "DHT.h"

// DISPLACED -> "settings.h"
/*
extern uint8_t dhtPin;
extern uint8_t dhtType;
*/

typedef struct {
    float h;// humidity
    float t;//  temperature as Celsius (the default)
    float f;//  temperature as Fahrenheit (isFahrenheit = true)
    float hif; // Compute heat index in Fahrenheit (the default)
    float hic; // Compute heat index in Celsius (isFahreheit = false)
    bool isValid;
} DHTData;


class DHTReader
{
    /*
    float h;// humidity
    float t;//  temperature as Celsius (the default)
    float f;//  temperature as Fahrenheit (isFahrenheit = true)
    float hif; // Compute heat index in Fahrenheit (the default)
    float hic; // Compute heat index in Celsius (isFahreheit = false)
    bool isValid;
    */
private:
    //DHT _dht(DHTPIN, DHTTYPE);
    DHT *_dht;

public:
    // TEST
    //int y;
    
    //void initDht(uint8_t pin, uint8_t type) {}
    DHTReader()    {
       _dht = 0;
    }  
  
    ~DHTReader()    {
        if(_dht)
            free(_dht);
        _dht = 0;
    }
  


    void beginReader(uint8_t pin, uint8_t type)    {
        if(_dht)
            delete _dht;
        _dht = new DHT(pin, type);
        _dht->begin();
    }
     
    DHTData readDHT();
};

#endif // USE_DHT_SENSOR
