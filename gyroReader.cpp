// file: gyroReader.cpp

#include "gyroReader.h"




//#if USE_GYRO_SENSOR_BNO055
//Adafruit_BNO055 *bno;
//#endif


#if USE_GYRO_SENSOR


void GYROReader::_init(uint8_t sdaPin, uint8_t sdcPin) {
  Serial.println("> GYROReader::_init()");
    
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin(sdaPin, sdcPin); // usually (D3, D4) if esp8266/MPU6050, test: (D4, D5) for BNO055
  //Wire.begin(D3, D4); // usually (D3, D4) if esp8266/MPU6050, test: (D4, D5) for BNO055
#if USE_GYRO_SENSOR_MPU6050
  Wire.setClock(100000);
  Wire.setClockStretchLimit(2 * 230);
#else
  Wire.setClockStretchLimit(1000); // Allow for 1000us of clock stretching
#endif



#if USE_GYRO_SENSOR_MPU6050
    delay(10);
    this->testGyro();
    // init the Accel
    _gyro->initialize();

    // only https://github.com/jarzebski/Arduino-MPU6050/blob/master/MPU6050.h
    //_gyro->calibrateGyro(); // doc? offsets docs?
#else

      /* Initialise the sensor */
    while(!_gyro->begin())
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!\n");
        delay(1000);
    }
#endif


#if USE_GYRO_SENSOR_MPU6050
  _gyro->setFullScaleAccelRange(MPU6050_ACCEL_FS_2);
  _gyro->setFullScaleGyroRange(MPU6050_GYRO_FS_250);
  _gyro->setDLPFMode(MPU6050_DLPF_BW_5);
  _gyro->setTempSensorEnabled(true);
//#ifdef USE_DMP
//  _gyro->setDMPEnabled(true);
//  packetSize = _gyro->dmpGetFIFOPacketSize();
//#endif
  _gyro->setInterruptLatch(0); // pulse
  _gyro->setInterruptMode(1);  // Active Low
  _gyro->setInterruptDrive(1); // Open drain
  _gyro->setRate(17);
  _gyro->setIntDataReadyEnabled(true);
#endif

///applyOffset();

  Serial.println("< GYROReader::_init()");
}

GYROData GYROReader::readGYRO()
{
    GYROData result;


#if USE_GYRO_SENSOR_MPU6050
    //Serial.print("(waiting status..");
    //Serial.printf("%x", _gyro);Serial.print("..");
    while (!_gyro->getIntDataReadyStatus())
        delay(2);
    //Serial.print(".ok)..");
    int16_t ax, ay, az;
    _gyro->getAcceleration(&ax, &ay, &az);
    int16_t gx, gy, gz;
    _gyro->getRotation(&gx, &gy, &gz);
    //accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    
    result.ax = ((float)ax) /16384.0; //if degrees
    result.ay = ((float)ay) /16384.0;
    result.az = ((float)az) /16384.0;
    
    result.tilt = result.calculateTilt();
    
    float rawTemperature = _gyro->getTemperature();
    float temperature = rawTemperature / 340.00 + 36.53;
    result.temperature = temperature;
    
    //Serial.printf(" readGYRO(), found (int): %i, %i, %i,  t: %.2f\n",  ax, ay, az, rawTemperature);
    //Serial.printf(" readGYRO(), found (float): %.2f, %.2f, %.2f, t: %.2f\n",  result.ax, result.ay, result.az, result.temperature);
    //Serial.printf(" readGYRO(), found (rotation): %i, %i, %i\n",  gx, gy, gz);
#else
  /* Get a new sensor event */
  sensors_event_t event;
  _gyro->getEvent(&event);

  /* Board layout:
         +----------+
         |         *| RST   PITCH  ROLL  HEADING
     ADR |*        *| SCL
     INT |*        *| SDA     ^            /->
     PS1 |*        *| GND     |            |
     PS0 |*        *| 3VO     Y    Z-->    \-X
         |         *| VIN
         +----------+
  */
/*
    // not usefull!!:
    result.gx = event.gyro.x;
    result.gy = event.gyro.y;
    result.gz = event.gyro.z;
 */   

    float roll = event.orientation.roll;        // .x, as in Adafruit_Sensor.h
    float pitch = event.orientation.pitch;      // .y
    float heading = event.orientation.heading;  // .z, (= "Yaw")
    Serial.print(" mesured -  ");
    Serial.print("  roll: "   ); Serial.print(roll);
    Serial.print(", pitch: "  ); Serial.print(pitch);
    Serial.print(", heading: "); Serial.print(heading); 
    Serial.println();

    // experimentally, in te position where Pins are at left, we need the Heading
    result.tilt = event.orientation.heading;
#endif

    result.isValid = true;
    return result;
}



bool GYROReader::testGyro()
{
    uint8_t res = Wire.status();
    if (res != I2C_OK)
        Serial.println(String(F("   I2C ERROR: ")) + res);

#if  USE_GYRO_SENSOR_MPU6050
    bool con = _gyro->testConnection();
    if (!con)
        Serial.println(F("   Acc Test Connection ERROR!"));
    return res == I2C_OK && con == true;
#else
    return res == I2C_OK;
#endif
}


float GYROData::calculateTilt()
{
  float _ax = ax;
  float _ay = ay;
  float _az = az;
  float pitch = (atan2(_ay, sqrt(_ax * _ax + _az * _az))) * 180.0 / M_PI;
  float roll = (atan2(_ax, sqrt(_ay * _ay + _az * _az))) * 180.0 / M_PI;
  tilt = sqrt(pitch * pitch + roll * roll);
  return tilt;
}



#endif // USE_GYRO_SENSOR
