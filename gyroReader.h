// file: gyroReader.h


#include "settings.h"
// TODO: TEMP
///#include "credentials.h"






#if USE_GYRO_SENSOR
//#
//#include "OneWire.h"
#include "Wire.h"

# if    USE_GYRO_SENSOR_MPU6050
#  include "MPU6050.h"
   // docs: https://os.mbed.com/users/moklumbys/code/MPU6050/docs/tip/classMPU6050.html
# elif  USE_GYRO_SENSOR_BNO055
#  include <Adafruit_Sensor.h>
#  include <Adafruit_BNO055.h>
#  include <utility/imumaths.h>
# endif



#ifndef uint8_t
//#include <types.h> // uint8_t; http://minirighi.sourceforge.net/html/types_8h-source.html
typedef unsigned char uint8_t;
#endif

///extern uint8_t gyroPin;
//extern uint8_t dhtType;

/*
#define MEDIANROUNDSMAX 49
#define MEDIANROUNDSMIN 29
#define MEDIANAVRG MEDIANROUNDSMIN
#define MEDIAN_MAX_SIZE MEDIANROUNDSMAX
*/

typedef struct {
    float   ax, ay, az;
    float   temperature;
    int16_t gx, gy, gz;
    
    float   tilt;
    
    bool    isValid;

    float calculateTilt();
    
} GYROData;

typedef struct {
    float   gdsAngle;
    unsigned long gdsTime;
} GYRODataToSyn;


class GYROReader
{
/*    float h;// humidity
    float t;//  temperature as Celsius (the default)
    float f;//  temperature as Fahrenheit (isFahrenheit = true)
    float hif; // Compute heat index in Fahrenheit (the default)
    float hic; // Compute heat index in Celsius (isFahreheit = false)
    bool isValid;
*/
private:
#if  USE_GYRO_SENSOR_MPU6050
    MPU6050         *_gyro;
#elif USE_GYRO_SENSOR_BNO055
    // Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28); // original BNO
    Adafruit_BNO055 *_gyro; // = Adafruit_BNO055(-1, 0x29, &Wire);    // CJMCU-055 
#endif

    void _init(uint8_t sdaPin, uint8_t sdcPin);

public:
    GYROReader()    {
#if  USE_GYRO_SENSOR_MPU6050
        _gyro = new MPU6050();
#elif USE_GYRO_SENSOR_BNO055
    // Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);             // original BNO
    // Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x29);             // CJMCU-055 
    // Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x29, &Wire);      // CJMCU-055 
    _gyro = new Adafruit_BNO055(-1, 0x29, &Wire);
#endif
    }
    ~GYROReader()    {
        delete _gyro;
    }
    
    void initReader(uint8_t sdaPin, uint8_t sdcPin)    {
        this->_init(sdaPin, sdcPin);
    }

    bool testGyro();
    
    GYROData readGYRO();
};

#endif // USE_GYRO_SENSOR
