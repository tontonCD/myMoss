// file: ESP_WebServer.ino(.cpp)
//
// author: Christophe Delannoy
//

/*********
 * Works for both (automaticly) :
 * - ESP32
 * - ESP8266
 * Works both (see bellow):
 * - with an existing Network, or 
 * - as a stand-alone Access Point
 * 
 * primary sources :
 *   Rui Santos
 *   Complete project details at https://randomnerdtutorials.com  
 *   https://randomnerdtutorials.com/esp32-web-server-arduino-ide/
 *
 * added: Hot Spot deature
 *   https://randomnerdtutorials.com/esp32-access-point-ap-web-server/
*********/

/* please define MODE in "settings.h" */
#include "settings.h"
/* ***
 *  IP: open the Monitor, look for "IP address: xxx"
 * *** */
// TODO: TEMP
///#include "credentials.h"

/*
#if USE_DHT_SENSOR
#   include "DHT.h"
// Please define the Digital pin connected to the DHT sensor:
#   if   defined(ESP32  )   // <- the Board is (e.g.): "ESP32 Dev Module"
const uint8_t DHTPIN = 2;       
#   elif defined(ESP8266)   // <- the Board is (e.g.): "LOLIN(WEMO) D1 R2 & mini" 
const uint8_t DHTPIN = D2; // for D2, but D2 undefined here, consult datasheet     
#   endif
const uint8_t DHTTYPE = DHT22; // real type can be: "DHT 22", "AM2302", "AM2321"
#endif
*/



#include "ESP_WebServer.h" // MODE_SAP must be defined before

// TODO: still OBSO

/* DISPLACED, because conflicts (server/client)
#if USE_DHT_SENSOR
# include "dhtReader.h"
  extern DHTReader dhtReader;
  extern DHTData dhtData;
#elif USE_DHT_READER
#endif // USE_DHT_SENSOR


*/


// Variable to store the HTTP request
String header;

// TODO: client Only ?

// TEST
//#include <time.h>

void setup() {
    //Serial.begin(115200);
    Serial.begin(74880); // because at wakeup and before setup(), serial is 74880
    while (!Serial); // wait for Leonardo enumeration, WeMo too, others continue immediately
    delay(1000);


    // TODO: displace ?
/*
#if MODE_SERVER
    pinMode(serverBuiltinLedPin, OUTPUT);
#else
    pinMode(clientBuiltinLedPin, OUTPUT);
#endif
  */  

    Serial.println("-------------------------");
    Serial.println("*************************");
    Serial.println("Starting: ESP32_WebServer");
    Serial.print(  " mode: "); Serial.println(MODE_String);

    // TESTS
/*
    // test: printULongAsTime()
    unsigned long input;
    unsigned long output;
    String str;
    
*/

/* TEST: OK
    unsigned long input = 0xFFFFFFFF; // equals: 4294967295; 
    String str = String(input);
    unsigned long output = str.toInt();
    Serial.print("print as string: ");Serial.println(str);
*/

/*
    printULongAsTime(input);
    // 0xFFFFFFFF => 49 days, 49.17: 2:47.295
  
    input = 0x7FFFFFFF; // 2147483647=>0x7FFFFFFF
    str = String(input);
    output = str.toInt();
    printULongAsTime(output);
    Serial.println();
 */
    

    beginWifi();

    // TEST
    //https://nodemcu.readthedocs.io/en/dev/modules/rtctime/
    //time ttime = rtctime.get();
    // See also: sntp.sync()




    Serial.println();
}


void loop(){


    
// TODO: sould be all inside:
#if MODE_SERVER
// server_Parse();
        server_checkClient();
# if MODE_SAP==false // TODO: displace
// server_SendPage();

# else
# endif
#else // ->CLIENT
    client_SendData();
# if USE_DEEPSLEEP
    // TODO:
# else

# endif
//    Serial.println("error"); // ???
#endif




    

} // loop

/* **** UTILS **** */
void printULongAsTime(unsigned long milliseconds) {
    Serial.printf("%u milliseconds => ", milliseconds );
    
    unsigned long  seconds = milliseconds / 1000;
    milliseconds -= (1000*seconds);
    
    unsigned long minutes = seconds /60;
    seconds -= (minutes*60);
    // e.g. 3661s => m=61, s=3661-3660=1

    unsigned long hours = minutes /60;
    minutes -= (hours*60);

    unsigned long days = hours /24;
    hours -= (days*24);
  
    printTwo(days);
    Serial.print(".");
    printTwo(hours);Serial.print(":");printTwo(minutes);Serial.print(":");printTwo(seconds); 
    Serial.print(".");
    printTwo(milliseconds);
    Serial.println();
}
void printTwo(int input)    {
    if(input<10)
        Serial.print(0);
    Serial.print(input);
}

unsigned long strToULong(String theString)    { // OBSO
    unsigned long result = 0;;

    const char *chars = theString.c_str();
    Serial.print("string: "); Serial.println(chars);
    Serial.print("chars:  "); Serial.println(chars);
    while(*chars) {
        if(*chars>='0' && *chars<='9')   {
            Serial.print(result);Serial.print(" * 10 + ");Serial.print(*chars-'0');Serial.print(" = ");
            result = 10*result + (*chars-'0');
            chars += 1;
            Serial.println(result);
        } /*
        else
        if(*chars=='1')
        {
            Serial.print(result);Serial.print(" * 10 + ");Serial.print("1");Serial.print(" => ");
            result = 10*result + 1;
            chars += 1;
            Serial.println(result);
        } */
        else {
            Serial.print("break because *chars = ");Serial.println(*chars); // on entry 130080l, prints "1" -> NO it's a "l" not "1"
            break;
        }
    }

    return result;
}
/* **** NOTES **** */
// https://github.com/esp8266/Arduino/blob/master/libraries/esp8266/examples/RTCUserMemory/RTCUserMemory.ino

void goodNight(uint32_t seconds)
{
    //ESP.rtcUserMemoryWrite(RTCSLEEPADDR, &left2sleep, sizeof(left2sleep));
    //bool rtcUserMemoryRead(uint32_t offset, uint32_t *data, size_t size);
    //bool rtcUserMemoryWrite(uint32_t offset, uint32_t *data, size_t size);
    // example in the ESP8266 Core for Arduino in RTCUserMemory.ino.

    //https://panther.kapsi.fi/posts/2019-05-14_esp8266_deep_sleep
    //a quick calculation ... shows it can sleep a maximum of approximately 71 minutes at a time.

/*
 extern "C" {
  #include "user_interface.h"
}
void setup() {
  byte rtcStore[2];
  system_rtc_mem_read(64, rtcStore, 2);
  system_rtc_mem_write(64, rtcStore, 2);
}    */

/*
 * https://github.com/esp8266/Arduino/issues/2875
 *  return system_rtc_mem_read(64 + offset, data, size);
    return system_rtc_mem_write(64 + offset, data, size);
 */

/* https://www.cours-gratuit.com/cours-arduino/esp8266-tutoriel-arduino-en-pdf
 *  La taille totale de la mémoire de l'utilisateur RTC est de 512 octets. 
 *  Par conséquent, l'offset + la taille de (données) ne doit pas dépasser 512
 *  Voir aussi :
 *   ESP.getVcc () peut être utilisé pour mesurer la tension d'alimentation. 
 *   ESP doit reconfigurer le CAN au démarrage pour que cette fonctionnalité soit disponible. 
 *   Ajoutez la ligne suivante en haut de votre dessin pour utiliser getVcc:
 *     ADC_MODE (ADC_VCC); 
 */
/* reprendre
 *  https://www.bakke.online/index.php/2017/06/24/esp8266-wifi-power-reduction-avoiding-network-scan/
 *  Avoiding network scan
 */
}
