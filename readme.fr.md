myMoss

## Résumé

Il s'agit d'une solution Client/Serveur, inspirée du système "iSpindle"^[lien TODO] permettant de mesurer la densité d'un brassin en cours de fermentation.

La construction matérielle ainsi que le concept détaillée dans mon [blog](Blog "myMoss" : http://pro.numericable.fr/chdelann/Programmation/mySpindle/myspindle.html) et utilise **2 x Wemo** dont l'une avec **Gyroscope** pour les mesures.

# Installation
Il est conseillé d'utiliser ```git clone```^[Sur Window, si ce n'est fait, installer GitWindow : https://git-scm.com/download/win)].

Dans le dossier final, duppliquer et renommer (ou simplement renommer)```credential_exemple.h``` en```credential.h```, et ```settings.h``` en ```settings.h``` ; ces fichiers ne seront pas écrasés lors de mises à jour.

Ignorer les autre fichiers.

## Exécution/Upload

Lancer Arduino.app^[télécharger arduino.app : https://www.arduino.cc/], normalement vous avez installé le support pour WeMo^[Support Wemo (et TTGO) pour Arduino.app : https://randomnerdtutorials.com/solved-arduino-ide-esp32-esp8266-installation/]

Editez le fichier ```settings.h```, 
- modifiez les valeurs "PIN" selon le matériel construit (```SDA_PIN``` et ```SDA_PIN```)

- choisissez le **mode Serveur** (chercher ```#define MODE xxx``` et remplacer par ```#define MODE modeServer_viaRouter```), choisissez (dans Arduino.app) **le Port** sur lequel se trouve la carte qui fonctionnera comme Serveur (chez moi "18"),

- Ouvrir le Moniteur Arduino, Faire **Run**, récupérer l'adresse (chercher  ```IP address:  ```, normalement  ```192.168.1.141 ``` mais dépend du processeur)
- vérifier la valeur assignée à ```serverAdress_LocalNetwork``` dans ```settings.h``` l'identifiant , la mettre à jour le cas échéant.
)

Normalement les deux parties, Clients et Serveur vont maintenant fonctionner.

- choisissez le **mode Client** (chercher ```#define MODE xxx``` et remplacer par ```#define MODE modeClient_NoDeepSleep```), choisissez (dans Arduino.app) **le Port** sur lequel se trouve la carte qui fonctionnera comme Serveur (chez moi "19"),

- faire **Run** ~~, récupérer l'adresse (chercher  ```IP address:  ```, normalement  ```192.168.1.141 ``` mais dépend du processeur)~~

- lancer un **Navigateur** et connectez-vous à l'adresse du Serveur (normalement [http://192.168.1.141](http://192.168.1.141)).

![[../html/images/pageWeb_noBuiltInLed.png]]

Il vous permettra de changer la configuration du Client. Ne cherchez pas le mode **DeepSleep** immédiatement (*note : non implémenté actuellemen*t) car il n'en sortira qu'après le Time choisi).
- l'adresse [192.168.1.141/data](http://192.168.1.141/data) vous permettra de vérifier que l'ensemble fonctionne.

Dans le deux cas, le client n'étant pas connecté en continu, il faudra **rafraichir la page** dans le navigateur pour voir :
1) les nouvelles données : toutes les  "Time" minutes
2) que les réglages ont été appliqués. Jusqu'à deux fois la valeur de "Time".



----

## Roadmap
- activer le mode **DeepSleep** (indispensable pour les économies de consommation et donc l'autonomie)
- tester la robustesse : 
  1) comparer les mesures par deux modules différents
  2) mesure sur la durée totale de fermentation d'un brassin (situation réelle) et comparer au réfractomètre.
  3) 
- 
## Historique de version (résumé)
(Version Log)
 
- 0.15: utilise désormais, comme Gyroscope, un modèle basé sur **BNO055** (par exemple CJMCU-055), qui permet des mesure "absolues" (voir les Notes du Blog).
- 0.14 : les échanges de données entre le Client et le Serveur fonctionnent parfaitement (sur des données utiles)
 
Voir aussi l'[historique complet](versionLog)




