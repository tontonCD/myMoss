// file: settings.h

#include "defines.h"



/* ********************************************** */
/* ***** MODE TO DEFINE ***** CLIENT/SERVER ***** */
/* can use for now: modeClient_NoDeepSleep // 3   */
/*                  modeServer_viaRouter   // 1   */
/* ********************************************** */
#define MODE modeServer_viaRouter


/* **************************** */
/* ***** SENSOR TO DEFINE ***** */
#define USE_GYRO_SENSOR_BNO055 true
#define USE_GYRO_SENSOR_MPU6050 false






/* note on: SOFT ACCESS POINT (defined as: MODE_SAP)
 * - if MODE_SAP (false), connects to a "LOCAL NETWORK" (a router), 
 *    - if a Server, the Server presents to the Router
 *    - if a Client, the client acceds to the Server via the Router,
 * - if MODE_SAP (true),  acts as a "Soft ACCESS POINT" (soft-AP or hotspot), 
 *    * this mode sould be used only for a Server
 *    * any Client will connect directly to the Server
 *   @see SmartConfig https://www.switchdoc.com/2018/06/tutorial-esp32-bc24-provisioning-for-wifi/
 */

/* *************************************** */
/* ***** CONSISTENCY ** DON'T CHANGE ***** */
/* *************************************** */
#if     MODE==modeServer_viaRouter
# define MODE_SERVER   true
# define MODE_SAP      false
# define USE_DEEPSLEEP false
#elif   MODE==modeServer_asSap
# define MODE_SERVER   true
# define MODE_SAP      true
# define USE_DEEPSLEEP false
#elif   MODE==modeClient_NoDeepSleep
# define MODE_SERVER   false
# define MODE_SAP      false
# define USE_DEEPSLEEP false
#elif   MODE==modeClient_withDeepSleep
# define MODE_SERVER   false
# define MODE_SAP      false
# define USE_DEEPSLEEP true
#else
#       error Choose one!
#endif
/* *************************************** */
/* ***** END *********** CONSISTENCY ***** */
/* *************************************** */





/* *****
 * usages defines:
 * - USE_DHT  (_SENSOR/_READER): if use an Humidity sensor, 
 *     currently DHT22 or DHT11,
 * - USE_GYRO (_SENSOR/_READER): if use a Gyroscope, 
 *     currently MPU6050,
 *   this defines has two ways, e.g. GYRO:
 *   - USE_GYRO_SENSOR if using a sensor,
 *   - USE_GYRO_READER if not using the sensor, but want to receive Gyro data
 *  ***** */

#if MODE_SERVER // -> SERVER (buffer)
# define USE_DHT_SENSOR    false // todo: could be clearer if using (e.g.) SERVER_USES__DHT_SENSOR / CLIENT_USES__DHT_SENSOR
# define USE_DHT_READER    true

# define USE_GYRO_SENSOR   false
# define USE_GYRO_READER   true

#else           // -> CLIENT  (myMoss)
# define USE_DHT_SENSOR    true
# define USE_DHT_READER    false

# define USE_GYRO_SENSOR   true
# define USE_GYRO_READER   false
#endif


#if USE_DHT_SENSOR // (LEGACY)
// Please define the Digital pin (DHTPIN) connected to the DHT sensor, and its type (DHTTYPE):

//# include "dhtReader.h"
//  DHTReader dhtReader;
//  DHTData dhtData;
  
# if   defined(ESP32  )     // <- the Board is (e.g.): "ESP32 Dev Module"
#  define DHTPIN    2            // const uint8_t
# elif defined(ESP8266)     // <- the Board is (e.g.): "LOLIN(WEMO) D1 R2 & mini" 
#  define DHTPIN    D2           // for D2, but D2 undefined here, consult datasheet     
# endif

# define  DHTTYPE   DHT22       // const uint8_t; real type can be: "DHT 22", "AM2302", "AM2321"

#endif // USE_DHT_SENSOR


#if    USE_GYRO_SENSOR_MPU6050 // as in "iSpindle"
# define SDA_PIN    D3              // Tx;      const uint8_t
# define SDC_PIN    D4              // SCL, Rx; const uint8_t
#elif  USE_GYRO_SENSOR_BNO055  // as in "myMoss"
//# define SDA_PIN    D3
//# define SDC_PIN    D4
// TEST -> OK; 
# define SDA_PIN    D1 // Tx
# define SDC_PIN    D2 // SCL, Rx
// TODO: valid for WeMo (ESP8266),  check for ESP32??
#endif

 
