# myMoss

<br>

## Version Log

### 0.15
- added support for BNO055!
- Client, FIX : permutation between "myMossID: " and "angle: " in the message sent,
- test (Wire): use (D1, D2) instead of (D3, D4) -> OK

**Works perfectly with the BNO055 based gyroscope**
<br>

### 0.14:
- changing the Server response, ~~checking UserAgent, and~~ moving data to a '``data: {}``' in the http header
- added log function : print_escapingCRLF(), for parameters (String) or (const char*)
- **remove the builtIn Led acces**, because the pin used is **D4**, juste like the Gyro sensor and could cause an infinite loop when attempting to read the sensor. 
  ...just added '``#if USE_BUILT_IN_LED``' for an other future hardware assembly.
- Server : fixed the display for the *Client deepSleep Time* in the HTML ConfigurationPage
- Server : Fix (again) with the httpHeader

**Exchanging data works perfectly in <u>both</u> ways Server <--> Client**
<br>

### 0.13:
- Server: fix serving the html page,
- Server: fix serving /data 
- Server: send to HTML the real Server_Received message (from sensor),
- Server, fix: added recognition for ``"%20"`` to ``skipSpaces()``, (case : "key: value" <u>from web</u>)
- Server, Web commands: capture all of {/bion, /bioff, /deepsleep:value, /calibration}

### 0.12:
- Fix on parsing (removing spaces after ":")
- refactored and generalized parsing with key as parameter
- defines messages structs for: Client->Server, Server->Message, for each in version To or From
- added ESP_WebServer_Common.cpp
- Fix: the Client workflow was broken

### 0.11:
- splited ESP_WebServer.cpp in ESP_WebServer_Server.cpp and ESP_WebServer_Client.cpp
- Server: factorized sendind of data and html
- Server/Client, Fix: builInLedState initial value
- renamed *credential_exemple.h* to *credential.exemple* (<u>doesn't appear anymore in the IDE</u>)

### 0.10
- fixed data separator (replaced "**=**" with "**:**"); 
- works now with {angle, time, **module identifier**}

### 0.9
- refactored Client code,
- refactored Server code,
- disctinction USE_GYRO_SENSOR/USE_GYRO_READER
added: Server sends it's ID (via User-ID:)

### 0.8
- bug fixes

### 0.7
- client send data to server, server display the last

----
## Issues (to_do)

### important (requiered)
- ~~faire un readme.md~~
- ~~Server: should not send the full HTML page when received "/setData", only for "/data"~~
- tester alim par broche Vcc, en 3.3V : mesurer la tenson fournie au gyroscope
- ~~client : si le serveur n'est pas joignable la 1e fois, il n'est pas connecté les suivantes~~
- Gyro calibration function! : OBSO
- interface (Client Setting) : can change the WiFi power for lower consumption); 
- can use blueTooth?
- interface (Client Setting) : launch DeepSleep mode


### improvements
- client: if ServerTimeout => logger + save data for later
	else mark data
- interface (Client Setting) : use a flag on Server to make the Client can exit from the DeepSleep mode


### miscelaneous
- ~~error msg (gyro),alors qu'il fonctionne : 
  chercher : MPU6050 testConnection()~~
- iPad application; Android?

<br>

- Server: save multiple data (until cleared from application)
- stock multiple data on Client (if Server is not reachable); 
  synchro each one when possible and clean after synchro
- fonction "clean" (server) ; doit être "on-demand"